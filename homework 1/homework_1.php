<?php

#task 1
function check_negative (int $num)
    {
        if ($num < 0){
            return "Данное число отрицательное";
        }elseif ($num == 0) {
            return "Данное число нейтральное";
        }else{
            return "Данное число положительное";
        }
}

echo check_negative(-5)  . PHP_EOL;


#task 2
$sentence = "All good things must come to an end";
echo strlen(trim(str_replace(" ","",$sentence)))  . PHP_EOL ;

#task 3
$sentence_2 = "All good things must come to an end";
if (str_contains($sentence_2, "a")){
    echo "Да" . PHP_EOL;
}else{
    echo "Нет" . PHP_EOL;
}

#task 4 & 5
function division(int $num){
    if ((($num%2)==0) && (($num%3)==0) && (($num%5)==0) && (($num%6)==0) && (($num%9)==0)){
        return $num . " - это число делится на 2, на 3, на 5, на 6, на 9 без остатка";
    }elseif((($num%3)==0) && (($num%5)==0) && (($num%7)==0) && (($num%11)==0)){
        return $num . " - это число делится на 3, на 5, на 7, на 11 без остатка";
    }else{
        return "Вы ввели некорректную цифру";
    }
}

echo division(1155) . PHP_EOL;

#task 6 & 7
$str = "All good things must come to an end";
$last = substr($str,-1);
echo $last . PHP_EOL;

#task 8
function triangle_area($arr){
    $a = $arr[0];
    $b = $arr[1];
    $c = $arr[2];
    
    $s = ($a + $b + $c) / 2;

    $area = ($s*($s-$a)*($s-$b)*($s-$c))**0.5; 
    return $area;
}

echo triangle_area([5,6,7]) . PHP_EOL;

#task 9
function square_area($array){
    $a = $array[0];
    $b = $array[1];

    $area = $a * $b;
    return $area;
}

echo square_area([5,6]) . PHP_EOL;

#task10
$number = 5;
echo ($number * $number) . PHP_EOL;

?>
